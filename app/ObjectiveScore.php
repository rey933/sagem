<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectiveScore extends Model
{
    protected $table = 'objective_score';
}
