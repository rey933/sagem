<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\DateMe\DateMeDateConverter;

class Objective extends Model
{
    protected $fillable = [
    	'evaluation_id', 'title', 'description', 'goal', 'scope', 'from_date', 'to_date', 'weight'
    ];

    public function fromQuarter()
    {    	
    	$quarter = new DateMeDateConverter($this->from_date);
    	return $quarter->dateToQuarter();
    }

    public function toQuarter()
    {    	
    	$quarter = new DateMeDateConverter($this->to_date);
    	return $quarter->dateToQuarter();
    }

    public function result()
    {
        return $this->belongsToMany(ObjectiveScore::class, 'objective_results', 'objective_id', 'score_id')
                    ->withPivot('result');                
    }
}
