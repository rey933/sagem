<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * Get the actual Employee User
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    /**
     * Get an Instance of the Team Employee
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
    	return $this->belongsTo(Team::class);
    }

    /**
     * Get all Employee Evaluations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }

    /**
     * Get the Employee supervisor
     * @return Employee
     */
    public function supervisor()
    {
        $supervisor = $this->where('sap_id', '=', $this->supervisor_sap_id)
                      ->first();
        return $supervisor;
    }

    public function getFormatedIdentity()
    {
        if(strlen($this->identity) <= 7) return $this->identity = '0'.$this->identity;
        return $this->identity;
    }

    public function getLastName()
    {
        return substr($this->name, 0, strpos($this->name, ' '));
    }

    public function getFirstName()
    {
        $string = substr($this->name, strpos($this->name, ",") + 1);

    }

    /**
     * Check if the actual User Employee is a Supervisor
     * @return bool
     */
    public function isSupervisor()
    {
        if($this->where('supervisor_sap_id', '=', $this->sap_id)->count() > 0) return true;
        return false;                
    }

    /**
     * Get the Employees of the actual supervisor
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function supervisedEmployees()
    {
        $employees = $this->where('supervisor_sap_id', '=', $this->sap_id)
                          ->get();
        return $employees;
    }

    /**
     * Check if the current Employee is the logged user
     * @param $employeeIdToCompare
     * @return bool
     */
    public function isMe($employeeIdToCompare)
    {
        if($this->id == $employeeIdToCompare) return true;
        return false;
    }

    /**
     * Check if the Employee have Evaluations
     * @return bool
     */
    public function haveAnOpenEvaluation()
    {
        $evaluations = $this->evaluations()->get();
        $haveOpenEvaluation = false;
        foreach ($evaluations as $evaluation) {
            if($evaluation->isOpen()) $haveOpenEvaluation = true;
        }
        return $haveOpenEvaluation;
    }

    /**
     * Get an instance of the currently open Evaluation
     * @return Evaluation
     */
    public function openEvaluation()
    {
        $evaluations = $this->evaluations()->get();
        foreach ($evaluations as $evaluation) {
            if($evaluation->isOpen()) return $evaluation;
        }
    }
}
