<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = [
        'employee_id', 'supervisor_id'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function result()
    {
        return $this->hasOne(EvaluationResult::class);
    }

    public function isOpen()
    {
        //If the current Evaluation have no results on the "evaluation_result" table it's open
        if($this->result()->get()->count() == 0) return true;
        return false;
    }
}
