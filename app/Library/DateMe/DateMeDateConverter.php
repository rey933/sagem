<?php

namespace App\Library\DateMe;

class DateMeDateConverter 
{
	private $date;
	private $year; //The $date year
	private $quarter;	

	public function __construct($date) 
	{	
		$this->year = substr($date, 0, 5);
		$this->date = $this->year.substr($date, 6);		
	}

	private function transformToQuarter()
	{
		if(strtotime($this->date) <= strtotime($this->year.'03-31 11:59:59')) {
			$this->quarter = 'Q1';
		} else if(strtotime($this->date) <= strtotime($this->year.'06-30 11:59:59')) {
			$this->quarter = 'Q2';
		} else if(strtotime($this->date) <= strtotime($this->year.'09-30 11:59:59')) {
			$this->quarter = 'Q3';
		} else if(strtotime($this->date) <= strtotime($this->year.'12-31 11:59:59')) {
			$this->quarter = 'Q4';
		}
	} 

	public function dateToQuarter()
	{
		$this->transformToQuarter();
		return $this->quarter;
	}
}