<?php

namespace App\Library\DateMe;

class DateMeQuarterConverter 
{
	private $quarter;
	private $fromDate;
	private $toDate;	

	public function __construct($quarter) 
	{
		$this->quarter = $quarter;
	}

	private function transformToDate()
	{
		switch ($this->quarter) {
			case 'Q1':
				$this->fromDate = now()->year . '-01-01 00:00:00';
				$this->toDate = now()->year . '-03-31 11:59:59'; 				
				break;
			case 'Q2':
				$this->fromDate = now()->year . '-04-01 00:00:00';
				$this->toDate = now()->year . '-06-30 11:59:59';
				break;
			case 'Q3':
				$this->fromDate = now()->year . '-07-01 00:00:00';
				$this->toDate = now()->year . '-09-30 11:59:59';
				break;
			case 'Q4':
				$this->fromDate = now()->year . '-10-01 00:00:00';
				$this->toDate = now()->year . '-12-31 11:59:59';
				break;
		}
	} 

	public function quarterFromDate()
	{
		$this->transformToDate();
		return $this->fromDate;
	}

	public function quarterToDate()
	{
		$this->transformToDate();
		return $this->toDate;
	}
}