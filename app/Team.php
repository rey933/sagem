<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
      'organizational_unity', 'title'
    ];

    public function members()
    {
    	return $this->hasMany(Employee::class);
    }
}
