<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectiveResult extends Model
{
    protected $table = 'objective_results';
}
