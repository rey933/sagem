<?php

namespace App\Http\Controllers;
use App\Employee;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
	/**
	 * Get an Employee
	 * @return Response
	 */
    public function index(Request $request)
    {
    	$employee = Employee::where('identity', '=', $request->identity)->first();
    	if($employee) {    		
	    	return response()->json([
	    		'http_code' => 200,                
	    		'employee' => [    			
	    			'identity' => $employee->identity,
	    			'sap_id' => $employee->sap_id,
	    			'name' => $employee->name,
                    'have_user' => $employee->user()->count() > 0 ? true : false,
	    		]
	    	]);
    	}
    	
    	return response()->json([
    		'http_code' => 404
    	]);
    }

    /*
     * Store an Employee
     */
    public function store(Request $request)
    {
    	return $request->all();
    }
}
