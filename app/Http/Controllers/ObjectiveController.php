<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Objective;
use App\ObjectiveScore;
use App\Library\DateMe\DateMeQuarterConverter;

class ObjectiveController extends Controller
{
	public function index($evaluation)
	{		
		$data = [];
		$objectives = Objective::where('evaluation_id', '=', $evaluation)->get();
		foreach($objectives as $objective) {
			$data[] = [
				'id' => $objective->id,
				'evaluation_id' => $objective->evaluation_id,
				'title' => $objective->title,
				'description' => $objective->description,
				'goal' => $objective->goal,
				'scope' => $objective->scope,
				'from_date' => $objective->fromQuarter(),
				'to_date' => $objective->toQuarter(),
				'weight' => $objective->weight,
				'result' => $objective->result()->get()->count() > 0 ? $objective->result()->first()->pivot->result : 0,
				'have_result' => $objective->result()->get()->count() > 0 ? true : false,
				'qualifying' => false,
			];
		}
		return $data;
	}


	/**
	 * Save an Objective in the Database
	 * @return \App\Objective
	 */
    public function store(Request $request)
    {
    	//Transforming Q1, Q2, Q3 and Q4 strings into dates    	
    	$firstQuarter = new DateMeQuarterConverter($request->newObjectiveData['from_date']);
    	$secondQuarter = new DateMeQuarterConverter($request->newObjectiveData['to_date']);
    	$objective = Objective::create([
    		'evaluation_id' => $request->evaluationId,
    		'title' => $request->newObjectiveData['title'],
    		'description' => $request->newObjectiveData['description'],
			'goal' => $request->newObjectiveData['goal'],
			'scope' => $request->newObjectiveData['scope'],
    		'from_date' => $firstQuarter->quarterFromDate(),
    		'to_date' => $secondQuarter->quarterToDate(),
    		'weight' => $request->newObjectiveData['weight']
    	]);

		return $objective;
    }

    public function delete($id)
    {    	
    	$objective = Objective::findOrFail($id);
    	$objective->delete();    	
    	return 'Deleted';
    }

    public function objectiveScores()
    {    	    	
    	$scores = ObjectiveScore::get();
    	return $scores;
    }

    public function addResults(Request $request)
    {    	
    	$objective = Objective::findOrFail($request->objective);
    	$score = ObjectiveScore::findOrFail($request->score);

    	$objective->result()->attach($score->id, ['result' => ($objective->weight * $score->value)]);
        $result = $objective->weight * $score->value;
    	return $result;
    }
}
