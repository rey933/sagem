<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function individualPerformanceReport()
    {
    	return view('reports.individual_performance');
    }

    public function teamPerformanceReport()
    {
    	return view('reports.team_performance');
    }
}
