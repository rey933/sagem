<?php

namespace App\Http\Controllers;

use App\Evaluation;
use App\Performance;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EvaluationController extends Controller
{
    public function me()
    {
        return view('evaluation.me');
    }

    public function team()
    {
        return view('evaluation.team');
    }

    public function store($supervisor_sap_id, $employee_id)
    {
        //Looking for Supervisor data
        $supervisor = Employee::where('sap_id', '=', $supervisor_sap_id)->first(['id'])->firstOrFail();
        //Creating Evaluation
        $evaluation = Evaluation::create([
            'employee_id' => $employee_id,
            'supervisor_id' => $supervisor->id
        ]);
        //Getting all Performance factors
        $performances = Performance::all();
        //Saving on Pivot table "Read: Controlling Evaluation Performance Factors"
        foreach($performances as $performance) {
            DB::table('evaluation_performance')->insert([
                'evaluation_id' => $evaluation->id,
                'performance_id' => $performance->id,
                'score_id' => 1
            ]);
        }
        //Redirecting to Workspace
        return redirect()->route('evaluation.workspace', ['evaluation' => $evaluation->id]);
    }

    public function watch($id)
    {
        $evaluation = Evaluation::findOrFail($id);
        $employee = $evaluation->employee()->first();
        return view('evaluation.watch', compact('employee', 'evaluation'));
    }

    public function workspace($id)
    {        
        $evaluation = Evaluation::findOrFail($id);
        $employee = $evaluation->employee()->first();
        return view('evaluation.workspace', compact('employee', 'evaluation'));
    }
}
