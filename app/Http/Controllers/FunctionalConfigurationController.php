<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FunctionalConfigurationController extends Controller
{
    public function index()
    {
        $users =  User::get();
    	return view('configuration.functional.index', compact('users'));
    }
}
