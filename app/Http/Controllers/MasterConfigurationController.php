<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterConfigurationController extends Controller
{
    public function index()
    {
        return view('configuration.master.index');
    }

    public function api()
    {
        return view('configuration.master.api');
    }
}
