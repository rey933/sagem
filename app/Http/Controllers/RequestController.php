<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function vacationRequest()
    {
    	return view('requests.vacation');
    }

    public function jobConstancyRequest()
    {
    	return view('requests.constancy');
    }
}
