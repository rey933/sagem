<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ObjectiveScore;

class ObjectiveScoreController extends Controller
{
    public function index()
    {
    	$scores = ObjectiveScore::get();
    	return $scores;
    }
}
