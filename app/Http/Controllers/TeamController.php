<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class TeamController extends Controller
{
    public function supervisedEmployees($supervisorId) 
    {    	
    	$supervisor = Employee::find($supervisorId);
    	return $supervisor->supervisedEmployees();
    }
}
