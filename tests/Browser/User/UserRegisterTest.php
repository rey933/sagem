<?php

namespace Tests\Browser;

use Tests\BrowserTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegisterTest extends BrowserTestCase
{
    use DatabaseMigrations;

    /**
     * An Employee can create a User.
     *
     * @return void
     */
    public function test_that_an_employee_can_create_a_user()
    {
        $team = $this->createTeam();
        $employee = $this->createEmployee([
            'team_id' => $team->id
        ]);
        $this->browse(function (Browser $browser) use ($employee){
            $browser->visit('/register')
                    ->type('identity', $employee->identity)
                    ->press('#search-employee-btn')
                    ->waitFor('#employee-found-alert')
                    ->assertSee($employee->name)
                    ->press('Siguiente')
                    ->press('Siguiente')
                    ->type('username', 'crey')
                    ->type('password', '025568')
                    ->type('password_confirmation', '025568')
                    ->press('Siguiente')
                    ->press('Crear Cuenta')
                    ->waitFor('#welcome-message')
                    ->assertSee('¡Registro Completado! Bienvenido a Self Management System.');
        });
    }
}
