<?php

namespace Tests\Browser;

use Tests\BrowserTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserLoginTest extends BrowserTestCase
{
    use DatabaseMigrations;

    /**
     * A Created User can make Login to the Application.
     *
     * @return void
     */
    public function test_that_a_user_can_make_login()
    {
        $team = $this->createTeam();     
        $user = $this->createDefaultUser('crey', $team->id);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/login')
                    ->type('username', $user->username)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->assertSee('Bienvenido a SMS - ' . $user->employee()->first()->name);
        });
    }
}
