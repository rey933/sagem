<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App;

class EmployeeTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->employee = factory(App\Employee::class)->create();
    }
    
    public function test_that_an_employee_have_a_user()
    {
        $team = factory(App\Team::class)->create(); 
        $user = factory(App\User::class)->create([
            'employee_id' => $this->employee->id,
            'team_id' => $team->id,
        ]);
        $this->assertCount(1, $this->employee->user()->get());
    }
}
