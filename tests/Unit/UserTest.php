<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /** @test*/
    public function test_that_a_user_belongs_to_an_employee()
    {
        $employee = factory(App\Employee::class)->create();
        $team = factory(App\Team::class)->create();  
        $user = factory(App\User::class)->create([
        	'employee_id' => $employee->id,
        	'team_id' => $team->id
        ]);
        $this->assertCount(1, $user->employee()->get());
    }
    
}