<?php

namespace Tests;
use App;

class BrowserTestCase extends DuskTestCase
{
	/**
	 * Creates a Team
	 * @return $team App\Team
	*/
    public function createTeam()
    {
        $team = factory(App\Team::class)->create();
        return $team;
    }

	/**
	 * Creates an Employee
	 * @return $team App\Employee
	*/
	public function createEmployee()
	{
		$employee = factory(App\Employee::class)->create();
		return $employee;
	}

	/**
	 * Creates a User
	 * @param string $userName, integer $teamId
	 * @return $team App\User
	*/
	public function createDefaultUser($userName, $teamId)
	{
		$employee = factory(App\Employee::class)->create();
        $user = factory(App\User::class)->create([
            'employee_id' => $employee->id,
            'team_id' => $teamId,
        	'username' => $userName        	
        ]);

        return $user;
	}
	
	/**
	 * Creates a Manager User for the given Team
	 * @param  integer $userId, integer $teamId
	 * @return $team App\Manager
	*/
	public function createTeamManager($userId, $teamId)
	{
		$manager = new App\Manager([
			'user_id' => $userId,
			'team_id' => $teamId
		]);

		return $manager;
	}
}