<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('website.index');
    return redirect('/login');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::prefix('team')->group(function() {
	Route::get('/', function() {
		return 'test';
	})->name('team');
	Route::get('/members', function() {
		return 'Members';
	})->name('team.members');
});

Route::prefix('evaluation')->group(function() {
    Route::get('my-evaluations', 'EvaluationController@me')->name('evaluation.me');
    Route::get('team-evaluations', 'EvaluationController@team')->name('evaluation.team');
    Route::get('store/{supervisor_sap_id}/{employee}', 'EvaluationController@store')->name('evaluation.store');
	Route::get('workspace/{evaluation}', 'EvaluationController@workspace')->name('evaluation.workspace');
    Route::get('watch/{evaluation}', 'EvaluationController@watch')->name('evaluation.watch');
});

Route::prefix('vacations')->group(function() {
    Route::get('request', 'RequestController@vacationRequest')->name('vacation.request');    
});

Route::prefix('constancy')->group(function() {
    Route::get('request', 'RequestController@jobConstancyRequest')->name('constancy.request');    
});

Route::prefix('reports')->group(function() {
    Route::get('individual-performance', 'ReportController@individualPerformanceReport')->name('individual.report');    
    Route::get('team-performance', 'ReportController@teamPerformanceReport')->name('team.report');
});

Route::prefix('configuration')->group(function() {
	Route::prefix('master')->group(function() {
		Route::get('/' , 'MasterConfigurationController@index')
			   ->name('configuration.master');
        Route::get('/api' , 'MasterConfigurationController@api')
            ->name('configuration.master.api');
	});
	Route::prefix('functional')->group(function() {
		Route::get('/' , 'FunctionalConfigurationController@index')
			   ->name('configuration.functional');
	});
});
