<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return response()->json([
    	'Making an Example'
    ]);
});

Route::middleware('auth:api')
	   ->prefix('objectives')
	   ->group(function() {
	//Change to ?
	Route::get('/{evaluation}', 'ObjectiveController@index')->name('objectives.index');
	Route::post('store', 'ObjectiveController@store')->name('objectives.store');
	Route::delete('delete/{objective}', 'ObjectiveController@delete')->name('objectives.delete');
	//Make a Refactoring
	Route::post('score', 'ObjectiveController@objectiveScores')->name('objectives.scores');
	Route::post('result', 'ObjectiveController@addResults')->name('objectives.results');		
});

Route::middleware('auth:api')
	   ->prefix('team')
	   ->group(function() {
	Route::get('supervised-employees/{supervisorId}', 'TeamController@supervisedEmployees')->name('team.supervised');		
});


Route::get('/employee', 'EmployeeController@index');
Route::put('/user/create', 'Auth\RegisterController@create');
