
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('search-employee', require('./components/User/SearchEmployee'));
Vue.component('create-new-account-wizard', require('./components/User/CreateNewAccountWizard'));
//Evaluation Module
Vue.component('evaluation-workspace', require('./components/Evaluation/EvaluationWorkspace'));
Vue.component('evaluation-objectives', require('./components/Evaluation/EvaluationObjectives'));
//Vue Form Wizard
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)
//Passport
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);
Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);
Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

//Vee-validate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

//Sweet Alert 2
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

//Charts
Vue.component('individual-performance-chart', require('./components/Charts/IndividualPerformance'));
Vue.component('team-performance-chart', require('./components/Charts/TeamPerformance'));

//VM object
const app = new Vue({
    el: '#app'
});




