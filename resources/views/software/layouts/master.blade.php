<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SAGEM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-lte.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        #loader {
            position: absolute;
            background: rgba(0, 0, 0, .5);
            z-index: 9999;
            width: 100%;
            height: 100%;
            display: none
        }
        #loader .circle{
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            position:absolute;
            left:50%;
            top:50%;
            margin-left:-60px;
            margin-top:-60px
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
    @yield('post_css')
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div id="loader">
    <div class="circle"></div>
</div>
<!-- Site wrapper -->
<div id="app" class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>GM</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>SAGEM</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="http://vecgupra25.plumrose.com/Fotos/{{Auth::user()->employee()->first()->getFormatedIdentity()}}.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->username }}</p>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Menú</li>
                <li>
                    <a href="{{ route('home') }}">
                        <i class="fa fa-home"></i> <span>Inicio</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-industry"></i>
                        <span>Evaluación de Desempeño</span>
                        <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ route('evaluation.me') }}"><i class="fa fa-child"></i> Mis Evaluaciones</a></li>
                        @if(Auth::user()->employee()->first()->isSupervisor())
                            <li><a href="{{ route('evaluation.team') }}"><i class="fa fa-group"></i> Mi Equipo</a></li>
                        @endif
                    </ul>
                </li>
                @if(Auth::user()->employee()->first()->isSupervisor())
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Reportes</span>
                        <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                       <li><a href="{{ route('individual.report') }}"><i class="fa fa-thermometer-three-quarters"></i> Rendimiento por Colaborador</a></li>
                       <li><a href="{{ route('team.report') }}"><i class="fa fa-vcard"></i> Rendimiento Promedio</a></li>                                          
                    </ul>
                </li>
                @endif                                
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-envelope-open"></i>
                        <span>Solicitudes</span>
                        <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                       <li><a href="{{ route('vacation.request') }}"><i class="fa fa-bed"></i> Vacaciones</a></li>
                       <li><a href="{{ route('constancy.request') }}"><i class="fa fa-vcard"></i> Constancia de Trabajo</a></li>                                           
                    </ul>
                </li>
                <!--<li>
                    <a href="#">
                        <i class="fa fa-gear"></i> <span>Configuraciones</span>
                    </a>
                </li>-->
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                       <i class="fa fa-grav"></i> <span>Salir</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('content-title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('box-title')</h3>
                </div>
                <div class="box-body">
                    @yield('box-body')
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    @yield('box-footer')
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('js/app.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>

@yield('post_script')
</body>
</html>
