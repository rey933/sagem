@extends('software.layouts.master')

@section('box-title')
	Rendimiento por Colaborador
@stop

@section('box-body')
	<individual-performance-chart supervisor="{{ Auth::user()->employee()->first()->id }}"></individual-performance-chart>
@stop