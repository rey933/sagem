@extends('software.layouts.master')

@section('box-title')
	Rendimiento de la Unidad
@stop

@section('box-body')
	<team-performance-chart supervisor="{{ Auth::user()->employee()->first()->id }}"></team-performance-chart>
@stop