<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - SAGEM</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        #banner {
            background: url("http://nutcache-www.uzbiuivh53t5rozh1auf0hrai.netdna-cdn.com/wp-content/uploads/2017/04/empoweremployees.jpg");
            background-size: cover;
            height: 100vh;
        }
        .right-col {
            
        }
            .right-col .login-form {
                margin: 300px auto;
                width: 450px;                
            }        
    </style>
</head>
<body>
    <div class="row">
        <!-- Banner Image -->
        <div class="col-sm-8" id="banner"></div>

        <div class="col-sm-4 right-col">
            <!-- Login Form -->
            <div class="login-form">
                <div class="row">
                    <div class="col">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label for="username" class="col-md-12 control-label">Nombre de Usuario</label>
                                        <div class="col-md-12">
                                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-2 control-label">Password</label>

                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Login
                                            </button>

                                            <a class="btn btn-link" href="{{ route('register') }}">
                                                Registro
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</body>
</html>
