@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Cuenta</div>
                <div class="panel-body">                    
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <search-employee by="identity"></search-employee>
                        
                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-8">
                                <button type="button" class="btn btn-default" disabled>
                                    Siguiente
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
