@extends('software.layouts.master')
@section('content-title')
    Evaluación de Desempeño <small>2017</small>
@endsection
@section('box-title')
    <a href="{{ route('evaluation.team') }}">Volver a Mi Equipo</a>
@stop
@section('box-body')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Nombre del Empleado</td>
                    <td>{{ $employee->name }}</td>
                </tr>
                <tr>
                    <td>Cédula de Identidad</td>
                    <td>{{ $employee->identity }}</td>
                </tr>
                <tr>
                    <td>Cargo</td>
                    <td>{{ $employee->position  }}</td>
                </tr>
                <tr>
                    <td>Unidad</td>
                    <td>{{ $employee->team()->first()->title }}</td>
                </tr>
                </tbody>
            </table>
            <hr>
            <evaluation-workspace evaluation="{{ $evaluation->id }}" view="false"></evaluation-workspace>
        </div>
    </div>
</div>
@endsection
