@extends('software.layouts.master')

@section('post_styles')
<style type="text/css">
    .employee-photo {
        width: 100px;
    }
</style>
@stop

@section('box-title')
    {{ Auth::user()->team()->title }}
@stop
@section('box-body')
    <div class="col-sm-8 col-sm-offset-2">
        <table class="table table-hover">
            @if(Auth::user()->employee()->first()->isSupervisor())
                <thead>
                <tr class="info">
                    <th><span class="ti-crown"></span> {{ Auth::user()->employee()->first()->name }}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach(Auth::user()->employee()->first()->supervisedEmployees() as $employee)
                    @if(! $employee->isMe(Auth::user()->employee()->first()->id))
                        <tr>
                            <td><img src="{{ asset('img/employee-images/'.$employee->getFormatedIdentity()) }}.jpg" class="img-rounded center-block" alt="User Image" width="50"></td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->position }}</td>
                            @if($employee->haveAnOpenEvaluation())
                                <td>
                                    <a href="{{ route('evaluation.workspace', ['evaluation' => $employee->openEvaluation()->id])  }}" class="btn btn-info btn-sm" title="Ir a Evaluación Abierta"><span class="fa fa-mail-forward"></span></a>
                                    <a href="#" class="btn btn-default btn-sm" title="Ir a Historial de Evaluaciones"><span class="fa fa-hourglass-end"></span></a>
                                </td>
                            @else
                                <td>
                                    <a href="{{ route('evaluation.store', ['employee' => $employee->id, 'supervisor_sap_id' => Auth::user()->employee()->first()->sap_id]) }}"
                                       class="btn btn-default btn-sm"
                                       title="Crear Evaluación para periodo en Curso">
                                        <span class="fa fa-copy"></span>
                                    </a>
                                    <a href="#" class="btn btn-default btn-sm" title="Ir a Historial de Evaluaciones"><span class="fa fa-hourglass-end"></span></a>
                                </td>
                            @endif

                        </tr>
                    @endif
                @endforeach

                </tbody>
            @else
                <thead>
                <tr class="info">
                    <th><span class="ti-crown"></span> {{ Auth::user()->employee()->first()->supervisor()->name }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ Auth::user()->employee()->first()->name }}</td>
                </tr>
                </tbody>
            @endif
        </table>
    </div>
@stop