@extends('software.layouts.master')

@section('box-body')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Mis Evaluaciones de Desempeño</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th>Año</th>
                    <th>Status</th>
                    <th>Resultado</th>
                    <th>Acciones</th>
                </tr>
                @foreach(Auth::user()->employee()->first()->evaluations()->get() as $evaluation)
                    <tr>
                        <td>{{ $evaluation->created_at->year }}</td>
                        <td>
                            @if($evaluation->isOpen())
                                <span class="badge bg-yellow">En Progreso</span>
                            @else
                                <span class="badge bg-green">Cerrada</span>
                            @endif
                        </td>
                        <td>
                            @if($evaluation->isOpen())
                                Sin Resultados
                            @else
                                95% - Alcanza Expectativas
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('evaluation.watch', ['id' => $evaluation->id]) }}" title="Ver Evaluación" class="btn btn-sm btn-default"><span class="fa fa-eye"></span></a>
                        </td>
                    </tr>
                @endforeach
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
@stop