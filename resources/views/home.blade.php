@extends('software.layouts.master')

@section('box-body')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>2</h3>
                            <p>Evaluaciones En Curso</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-thermometer-1"></i>
                        </div>
                        <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>0<sup style="font-size: 20px">%</sup></h3>

                            <p>Rendimiento 2017</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bar-chart"></i>
                        </div>
                        <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>6</h3>

                            <p>Objetivos En Curso</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-motorcycle"></i>
                        </div>
                        <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>3</h3>

                            <p>Usuarios</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-circle"></i>
                        </div>
                        <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div>
    </div>
</div>
@endsection
