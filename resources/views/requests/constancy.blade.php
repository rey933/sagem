@extends('software.layouts.master')

@section('box-title')
	Formulario de Solicitud de Vacaciones
@stop

@section('box-body')
	<form role="form">
	<div class="form-group">
        <label>Correo de recepción:</label>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-envelope"></i>
            </div>
            <input class="form-control pull-right" id="reservation" type="text" placeholder="Inserte el correo en el cual desea recibir su constancia.">
        </div>
        <!-- /.input group -->
    </div>
	</form>
@stop

@section('box-footer')
	<button type="submit" class="btn btn-primary">Solicitar</button>
@stop

@section('post_script')
<script type="text/javascript">
	$('#reservation').daterangepicker();
</script>
@stop