@extends('software.layouts.master')

@section('box-title')
	Formulario de Solicitud de Vacaciones
@stop

@section('box-body')
	<form role="form">
	<div class="form-group">
        <label>Fecha Deseada de Vacaciones:</label>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input class="form-control pull-right" id="reservation" type="text">
        </div>
        <!-- /.input group -->
    </div>
	</form>
@stop

@section('box-footer')
	<button type="submit" class="btn btn-primary">Solicitar</button>
@stop

@section('post_script')
<script type="text/javascript">
	$('#reservation').daterangepicker();
</script>
@stop