@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Configuración Maestra</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="#" class="thumbnail" title="Administración de Usuarios">
                                    <img src="{{url('img/content-images/functional-conf-1.png')}}" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-3" title="Base de Datos">
                                <a href="#" class="thumbnail">
                                    <img src="{{url('img/content-images/functional-conf-2.png')}}" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-3" title="Acceso - API">
                                <a href="{{ route('configuration.master.api')  }}" class="thumbnail">
                                    <img src="{{url('img/content-images/functional-conf-3.png')}}" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-3" title="Integración Continua">
                                <a href="#" class="thumbnail">
                                    <img src="{{url('img/content-images/functional-conf-4.svg')}}" alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection