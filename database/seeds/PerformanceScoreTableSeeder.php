<?php

use Illuminate\Database\Seeder;

class PerformanceScoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('performance_score')->insert([
            'title' => 'Sin Evaluar',
            'value' => 0,
            'created_at' => now()
        ]);
        DB::table('performance_score')->insert([
            'title' => 'No Alcanza el Mínimo Desempeño',
            'value' => 1,
            'created_at' => now()
        ]);
        DB::table('performance_score')->insert([
            'title' => 'Alcanza el Mínimo Desempeño',
            'value' => 2,
            'created_at' => now()
        ]);
        DB::table('performance_score')->insert([
            'title' => 'Su desempeño es satisfactorio',
            'value' => 3,
            'created_at' => now()
        ]);
        DB::table('performance_score')->insert([
            'title' => 'Su desempeño es muy satisfactorio',
            'value' => 4,
            'created_at' => now()
        ]);
    }
}
