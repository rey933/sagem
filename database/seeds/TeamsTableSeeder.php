<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = DB::connection('pcs_web')
                    ->table('HR_EMPLEADOS')
                    ->select(['UnidadOrg', 'Sociedad', 'Departamento'])
                    ->distinct('UnidadOrg')
                    ->whereNotNull('UnidadOrg')
                    ->get();
        foreach ($teams as $team) {
            DB::table('teams')->insert([
                'society' => $team->Sociedad,
                'organizational_unity' => $team->UnidadOrg,
                'title' => $team->Departamento,
                'created_at' => now(),
            ]);
        }
    }
}
