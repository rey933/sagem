<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TeamsTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        $this->call(ObjectiveScoreTableSeeder::class);
        $this->call(PerformanceTableSeeder::class);
        $this->call(PerformanceScoreTableSeeder::class);
    }
}
