<?php

use Illuminate\Database\Seeder;

class PerformanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('performances')->insert([
            'title' => 'Autonomía',
            'created_at' => now()
        ]);
        DB::table('performances')->insert([
            'title' => 'Iniciativa',
            'created_at' => now()
        ]);
        DB::table('performances')->insert([
            'title' => 'Trabajo en Equipo',
            'created_at' => now()
        ]);
        DB::table('performances')->insert([
            'title' => 'Cumplimiento de Normas',
            'created_at' => now()
        ]);
    }
}
