<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = DB::connection('pcs_web')
                        ->table('HR_EMPLEADOS')
                        ->where('Fecha', '=', '99991231') // Take just the active records
                        ->whereNotNull('UnidadOrg')
                        ->get();
        foreach($employees as $employee) {
            //Get the team id of the Employee
            $team = App\Team::where('organizational_unity', '=', $employee->UnidadOrg)->first(['id']);
            //Check if the first digit of Employee Identity is 0
             //If it's the case, remove that number
            if(substr($employee->CI, 0, 1) == '0') $employee->CI = substr($employee->CI, 1);
            DB::table('employees')->insert([
                'team_id' => $team->id,
                'supervisor_sap_id' => str_replace('.0', '', $employee->N_Pers_Sup),
                'sap_id' => str_replace('.0', '', $employee->N_Pers),
                'name' => $employee->Nombre_Emp,
                'identity' => $employee->CI,
                'email' => $employee->Mail,
                'position' => $employee->Cargo,
                'created_at' => now(),
            ]);
        }
    }
}
