<?php

use Illuminate\Database\Seeder;

class ObjectiveScoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('objective_score')->insert([
            'title' => 'No Cumplió',
            'value' => 0,
            'created_at' => now()
        ]);
        DB::table('objective_score')->insert([
            'title' => 'Mínima Expectativas',
            'value' => 0.5,
            'created_at' => now()
        ]);
        DB::table('objective_score')->insert([
            'title' => 'Alcanza Expectativas',
            'value' => 0.8,
            'created_at' => now()
        ]);
        DB::table('objective_score')->insert([
            'title' => 'Supera Expectativas',
            'value' => 1,
            'created_at' => now()
        ]);
        DB::table('objective_score')->insert([
            'title' => 'Excede Expectativas',
            'value' => 1.2,
            'created_at' => now()
        ]);
    }
}
