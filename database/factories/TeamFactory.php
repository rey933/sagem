<?php



/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Team::class, function (Faker\Generator $faker) {

    return [
    	'society' => rand(000, 999),
        'organizational_unity' => rand(1000, 99999999),
        'title' => $faker->title(),
    ];
});
