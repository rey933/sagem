<?php



/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Employee::class, function (Faker\Generator $faker) {

    return [
    	'sap_id' => rand(1000, 99999999),
    	'team_id' => 1,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'identity' => rand(0, 99999999),
    ];
});
