<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->unsigned(); //Team Where the Employee Work
            $table->foreign('team_id')
                  ->references('id')
                  ->on('teams');
            $table->string('supervisor_sap_id'); //Employee Supervisor Sap Code
            $table->string('sap_id')->unique(); //Personal Id on SAP
            $table->string('name');            
            $table->string('identity');
            $table->string('email')->nullable();
            $table->string('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
