<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objectives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evaluation_id')->unsigned();
            $table->foreign('evaluation_id')
                  ->references('id')
                  ->on('evaluations');
            $table->string('title');
            $table->string('description');
            $table->string('goal'); //Objective goal
            $table->string('scope');
            $table->dateTime('from_date'); //Evaluation starts
            $table->dateTime('to_date'); //Evaluation End
            $table->double('weight'); //Percentage weight of the Objective
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objectives');
    }
}
