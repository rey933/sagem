<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evaluation_id')->unsigned();
            $table->foreign('evaluation_id')
                  ->references('id')
                  ->on('evaluations');
            $table->integer('performance_id')->unsigned();
            $table->foreign('performance_id')
                  ->references('id')
                  ->on('performances');
            $table->integer('score_id')->unsigned();
            $table->foreign('score_id')
                  ->references('id')
                  ->on('performance_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_performance');
    }
}
